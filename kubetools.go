/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>
This file is part of {{ .appName }}.
*/
package main

import "bitbucket.org/chenyongguang/kubetools/cmd"

func main() {
	cmd.Execute()
}
