package web

import (
	_ "bitbucket.org/chenyongguang/kubetools/routers"
	"github.com/astaxie/beego"
)

func StartWebServer() {
	beego.BConfig.WebConfig.Session.SessionOn = true
	beego.Run()
}
