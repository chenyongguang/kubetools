package controllers

import (
	"github.com/astaxie/beego"
)

type AboutController struct {
	beego.Controller
}

func (c *AboutController) Get() {
	if beego.AppConfig.String("basepath") != "" {
		c.Data["basepath"] = beego.AppConfig.String("basepath")
	}
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.Layout = "templates/base.tpl"
	c.TplName = "about.html"
}
