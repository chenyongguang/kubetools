package controllers

import (
	"bitbucket.org/chenyongguang/kubetools/utils"
	"github.com/astaxie/beego"
	"github.com/spf13/viper"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	if beego.AppConfig.String("basepath") != "" {
		c.Data["basepath"] = beego.AppConfig.String("basepath")
	}

	if c.GetSession("user") != "admin" {
		c.Ctx.Redirect(302, beego.AppConfig.String("basepath")+"/login")
	}

	c.Data["namespaces"] = viper.GetStringSlice("targetNamespaces")
	c.Layout = "templates/base.tpl"
	c.TplName = "home.html"
}

func (c *MainController) Post() {
	c.Data["namespaces"] = viper.GetStringSlice("targetNamespaces")
	ns := c.GetString("namespace")

	action := c.GetString("action")
	c.Layout = "templates/base.tpl"

	if beego.AppConfig.String("basepath") != "" {
		c.Data["basepath"] = beego.AppConfig.String("basepath")
	}

	switch action {
	case "list":
		clientset := utils.GetKubeClientset()
		deployments, err := utils.ListDeployments(clientset, ns)
		if err != nil {
			c.Data["error"] = err
			c.TplName = "error.html"
		} else {
			c.Data["deployments"] = deployments

			c.Data["namespace"] = ns
			c.TplName = "home.html"
		}

	case "update":
		deploymentName := c.GetString("deploymentName")
		image := c.GetString("image")
		replicas, err := c.GetInt32("replicas")
		if err != nil {
			c.Data["error"] = err
			c.TplName = "error.html"
		} else {
			// TODO: update web logic
			beego.BeeLogger.Info("Prepare update -> namespace: %s, deploy: %s, image: %s, replicas: %d", ns, deploymentName, image, replicas)
			deployment := utils.Deployment{Name: deploymentName, Namespace: ns, Replicas: replicas, Image: image}

			clientset := utils.GetKubeClientset()
			err := utils.UpdateDeployment(clientset, deployment)
			if err != nil {
				c.Data["error"] = err
				c.TplName = "error.html"
			} else {
				c.Data["namespace"] = ns
				c.TplName = "home.html"
			}
		}

	default:
		c.Data["error"] = "No action define. "
		c.TplName = "error.html"
	}

}
