package controllers

import (
	"github.com/astaxie/beego"
)

type LogoutController struct {
	beego.Controller
}

func (c *LogoutController) Get() {
	c.DestroySession()
	c.Ctx.Redirect(302, beego.AppConfig.String("basepath")+"/login")
}
