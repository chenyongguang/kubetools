package controllers

import (
	"github.com/astaxie/beego"
)

type LoginController struct {
	beego.Controller
}

func (c *LoginController) Get() {
	if beego.AppConfig.String("basepath") != "" {
		c.Data["basepath"] = beego.AppConfig.String("basepath")
	}
	c.Layout = "templates/base.tpl"
	c.TplName = "login.html"
}

func (c *LoginController) Post() {
	if beego.AppConfig.String("basepath") != "" {
		c.Data["basepath"] = beego.AppConfig.String("basepath")
	}

	if c.GetString("adminEmail") != beego.AppConfig.String("adminEmail") {
		c.Data["error"] = "Invalid User!"
	} else if c.GetString("accessToken") != beego.AppConfig.String("accessToken") {
		c.Data["error"] = "Invalid Token!"
	} else {
		c.SetSession("user", "admin")
		c.Ctx.Redirect(302, beego.AppConfig.String("basepath")+"/")
	}

	c.Layout = "templates/base.tpl"
	c.TplName = "login.html"
}
