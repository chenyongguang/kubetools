package utils

import (
	"os"

	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

type Deployment struct {
	Name      string
	Namespace string
	Replicas  int32
	ReadyPods int32
	Image     string
}

func buildOutOfClusterConfig() (*rest.Config, error) {
	kubeconfigPath := os.Getenv("KUBECONFIG")
	if kubeconfigPath == "" {
		kubeconfigPath = os.Getenv("HOME") + "/.kube/config"
	}
	return clientcmd.BuildConfigFromFlags("", kubeconfigPath)
}

// GetClient returns a k8s clientset to the request from inside of cluster
func getClient() kubernetes.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Fatalf("Can not get kubernetes config: %v", err)
	}

	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalf("Can not create kubernetes client: %v", err)
	}

	return *client
}

// getClientOutOfCluster returns a k8s clientset to the request from outside of cluster
func getClientOutOfCluster() kubernetes.Clientset {
	config, err := buildOutOfClusterConfig()
	if err != nil {
		log.Fatalf("Can not get kubernetes config: %v", err)
	}

	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalf("Can not create kubernetes client: %v", err)
	}

	return *client
}

// GetKubeClientset
func GetKubeClientset() kubernetes.Clientset {
	var clientset kubernetes.Clientset
	_, err := rest.InClusterConfig()
	if err != nil {
		clientset = getClientOutOfCluster()
	} else {
		clientset = getClient()
	}
	return clientset
}

// ListDeployments: List all deployments in a dedicate namespace
func ListDeployments(clientset kubernetes.Clientset, namespace string) ([]Deployment, error) {
	log.Printf("Listing deployments in namespace %q:\n", namespace)
	deploymentsClient := clientset.AppsV1().Deployments(namespace)
	list, err := deploymentsClient.List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	var deployments []Deployment
	for _, d := range list.Items {
		log.Printf(" * %s (%d replicas) ready  %d, image: %s \n", d.Name, *d.Spec.Replicas, d.Status.ReadyReplicas, d.Spec.Template.Spec.Containers[0].Image)
		deployment := Deployment{}
		deployment.Name = d.Name
		deployment.Namespace = namespace
		deployment.Replicas = *d.Spec.Replicas
		deployment.ReadyPods = d.Status.ReadyReplicas
		deployment.Image = d.Spec.Template.Spec.Containers[0].Image
		deployments = append(deployments, deployment)
	}

	return deployments, nil
}

func UpdateDeployment(clientset kubernetes.Clientset, deployment Deployment) error {
	log.Printf("Patch deployment(%q) in namespace %q:\n", deployment.Name, deployment.Namespace)
	deploymentsClient := clientset.AppsV1().Deployments(deployment.Namespace)
	newDeployment, err := deploymentsClient.Get(deployment.Name, metav1.GetOptions{})
	if err != nil {
		return err
	}
	newDeployment.Spec.Template.Spec.Containers[0].Image = deployment.Image
	newDeployment.Spec.Replicas = &deployment.Replicas
	newDeployment, err = deploymentsClient.Update(newDeployment)
	if err != nil {
		return err
	}

	return nil
}

func ListPodsWithLabelSelector(clientset kubernetes.Clientset, namespace string, selector string) []string {
	log.Printf("Listing pods in namespace %q:\n", namespace)
	podsClient := clientset.CoreV1().Pods(namespace)
	list, err := podsClient.List(metav1.ListOptions{LabelSelector: selector})
	if err != nil {
		panic(err)
	}

	// TODO: Return should be a List of pod
	for _, d := range list.Items {
		log.Printf(" * pods: %s \n", d.Name)
	}

	return nil
}
