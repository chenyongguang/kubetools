# kubetools

kubetools is tools set for application. which support runs on  out-cluster or in-cluster.

**Runs on shell**:  git clone && edit .kubetools.yaml,`kubetools  serve`

**Runs on docker**: `docker  run  -p 7080:7080 -v ./kubeconfig:/root/.kube/config -v .kubetools:/.kubetools -v ./conf/app.conf:/conf/app.conf  -d chenu2/kubetools:0.0.3`

**Runs on k8s**:

* edit RBAC.yaml, set namespace to target namespace
```
    subjects:
    - kind: ServiceAccount
        name: kubetools
        namespace: moop-test
```

* if youll use ingress-nginx 's subpath as proxy. Such: http://yourdomain/kubetools, you should edit configmap/app.conf, define `basepath=yoursubpath`, otherwise,**DON'T** set it.

* Then: `kubectl apply -f deploy/`
* Notice: if you update configmap, you should restart pod to take effect.
##  features
* update  deployment image
* scale deployment

