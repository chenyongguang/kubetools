/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>
This file is part of {{ .appName }}.
*/
package cmd

import (
	"log"

	"bitbucket.org/chenyongguang/kubetools/web"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Serve for inbound request",
	Long: `kubetools will start in terminal or inside cluster.

When start with outside cluster, it will connect to k8s cluster with ~/.kube/config or $KUBECONFIG,
When start in k8s you should create BRAC first.`,
	Run: func(cmd *cobra.Command, args []string) {
		port = viper.GetInt32("port")
		log.Printf("Listening on port %d", port)

		web.StartWebServer()
	},
}

var port int32

func init() {
	rootCmd.AddCommand(serveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	serveCmd.PersistentFlags().Int32VarP(&port, "port", "p", 1234, "listening port")
	// serveCmd.PersistentFlags().StringVarP(&author, "author", "a", "s chen", "Author name for copyright attribution")
	viper.BindPFlag("port", serveCmd.PersistentFlags().Lookup("port"))

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
