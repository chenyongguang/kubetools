/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>
This file is part of {{ .appName }}.
*/
package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "config command",
	Long:  `edit/update your config`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Printf("config called. Listening on port %d", port)
	},
}

func init() {
	rootCmd.AddCommand(configCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// configCmd.PersistentFlags().String("foo", "", "A help for foo")
	configCmd.PersistentFlags().Int32VarP(&port, "port", "p", 1122, "listening port")
	_ = viper.BindPFlag("port", configCmd.Flags().Lookup("port"))
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// configCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
