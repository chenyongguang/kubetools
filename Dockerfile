FROM golang:alpine AS builder

# RUN apt-get update && \
#     apt-get install -y --no-install-recommends build-essential && \
#     apt-get clean && \
RUN    apk add git  && \
             mkdir -p "$GOPATH/src/kubetools"

ADD . "$GOPATH/src/kubetools"

RUN cd "$GOPATH/src/kubetools" && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a --installsuffix cgo --ldflags="-s" -o /kubetools

FROM bitnami/minideb:stretch
# FROM scratch

COPY --from=builder /kubetools /kubetools
ADD ./views /views
ADD ./static /static
ADD ./conf /conf
# RUN mkdir -p /root/.kube
# COPY ./config  /root/.kube/config
# COPY ./.kubetools.yaml /.kubetools.yaml

ENTRYPOINT ["/kubetools","serve"]
